package pe.edu.upeu.examen2P.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.edu.upeu.examen2P.entity.Categoria;
import pe.edu.upeu.examen2P.services.CategoriaService;

@RestController
@RequestMapping("/api")
public class CategoriaRestController {
	@Autowired
	private CategoriaService categoriaService;
	
	@GetMapping("/categorias")
	public List<Categoria> readAll(){
		return categoriaService.findAll();
	}
	
	@GetMapping("categoria/{id}")
	public Categoria findById(@PathVariable(name = "id") Long id) {
		return categoriaService.findById(id);
	}
	
	@PostMapping("/categoria")
	public Categoria Add(@RequestBody Categoria categoria) {
		return categoriaService.Add(categoria);
	}
	
	@PutMapping("/categoria/{idcategoria}")
	public void update(Categoria categoria, @PathVariable(value = "idcategoria") long idcategoria) {
		categoriaService.Update(categoria, idcategoria);
	}
	
	@DeleteMapping("/categoria/{idcat}")
	public void delete(@PathVariable(name = "idcat") Long idcat) {
		categoriaService.delete(idcat);
	}
}
