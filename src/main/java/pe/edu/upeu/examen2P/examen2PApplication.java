package pe.edu.upeu.examen2P;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class examen2PApplication {

	public static void main(String[] args) {
		SpringApplication.run(examen2PApplication.class, args);
	}

}
