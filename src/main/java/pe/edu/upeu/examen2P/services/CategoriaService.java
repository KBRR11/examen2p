package pe.edu.upeu.examen2P.services;

import java.util.List;

import pe.edu.upeu.examen2P.entity.Categoria;

public interface CategoriaService {
	public List<Categoria> findAll();
	public Categoria findById(Long idcat);
	public Categoria Add(Categoria cat);
	public void Update(Categoria categoria,long idcategoria);
	public void delete(Long idcat);
}
