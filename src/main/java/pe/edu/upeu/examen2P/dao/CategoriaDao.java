package pe.edu.upeu.examen2P.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import pe.edu.upeu.examen2P.entity.Categoria;
@Repository
public interface CategoriaDao extends CrudRepository<Categoria, Long> {
	
}
